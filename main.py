#!/usr/bin/env python3

import json
import os
import sqlite3
import sys

import genanki
import random


def select_book(cursor: sqlite3.Cursor):
    cursor.execute("""SELECT Books.OID, Books.Title, Books.Authors, COUNT(Items.OID) FROM Books
                      JOIN Items ON Items.ParentID = Books.OID
                      JOIN TypeNames ON TypeNames.OID = Items.TypeID
                      WHERE TypeNames.TypeName = 'obj.book_mark'
                      GROUP BY Books.OID""")
    results = cursor.fetchall()
    for i, (book_id, title, author, notes) in enumerate(results):
        print("%s) %s - %s" % (i, author, title))

    while True:
        selected_book = input("Choose book: ")
        if not selected_book.isdigit():
            print("Not number, select book from list")
            continue
        selected_book_index = int(selected_book)
        if selected_book_index not in range(0, len(results)):
            print("Not number in range, select book from list")
            continue
        return results[selected_book_index]


def export_notes(cursor: sqlite3.Cursor, book_id):
    cursor.execute("""SELECT Items.OID as ID FROM Items
                      JOIN TypeNames ON Items.TypeID = TypeNames.OID
                      WHERE 
                      TypeNames.TypeName = 'obj.book_mark' AND 
                      Items.ParentID = ?""", (book_id,))
    item_ids = cursor.fetchall()

    pairs = []
    for (item_id,) in item_ids:
        cursor.execute("""SELECT Tags.Val FROM Tags
                          JOIN TagNames on TagNames.OID = Tags.TagID
                          WHERE
                          TagNames.TagName = 'bm.quotation' AND
                          Tags.ItemID = ?""", (item_id,))

        result = cursor.fetchone()
        if result is None:
            continue
        (mark_json_str,) = result
        mark_json = json.loads(mark_json_str)

        front = mark_json["text"]

        cursor.execute("""SELECT Tags.Val FROM Tags
                          JOIN TagNames on TagNames.OID = Tags.TagID
                          WHERE
                          TagNames.TagName = 'bm.note' AND
                          Tags.ItemID = ?""", (item_id,))

        result = cursor.fetchone()
        if result is None:
            continue
        (note_json_str,) = result
        note_json = json.loads(note_json_str)

        back = note_json["text"]

        pairs.append((front, back))

    return pairs


def open_db(filename):
    try:
        return sqlite3.connect(filename)
    except:
        return None


def build_deck(filename, name, pairs):
    new_deck = genanki.Deck(
        random.randint(100000000, 999999999),
        name
    )

    note_model = genanki.Model(
        random.randint(100000000, 999999999),
        'Simple Model',
        fields=[
            {'name': 'Front'},
            {'name': 'Back'},
        ],
        templates=[
            {
                'name': 'Forward',
                'qfmt': '{{Front}}',
                'afmt': '{{Back}}',
            },
        ])

    for (front, back) in pairs:
        new_note = genanki.Note(model=note_model, fields=[front, back])
        new_deck.add_note(new_note)

    genanki.Package(new_deck).write_to_file('%s.apkg' % filename)


def main(pocketbook_root_path):
    db_path = os.path.join(pocketbook_root_path, "system", "config/" "books.db")
    db = open_db(db_path)
    if db is None:
        print("Pocketbook metadata not found")
        return
    cursor = db.cursor()
    (book_id, title, author, notes) = select_book(cursor)
    pairs = export_notes(cursor, book_id)
    build_deck("%s - %s" % (title, author), "%s - %s" % (title, author), pairs)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: ")
    main(sys.argv[1])
